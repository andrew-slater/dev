import os
import re
import ntpath
import argparse
from google.cloud import storage
import boto3
import pandas as pd
from urllib import request


###PARSE ARGUMENTS###
Epilog = """Example usage: python3 align_runner.py"""
parser = argparse.ArgumentParser(description="sync sample fastq and NGS resource files", epilog=Epilog)
parser.add_argument("--cfgfile", action="store", dest='cfgfile',default='', metavar='<cfgfile>', help="Required, define the PATH of genome.cfg file")
# parser.add_argument("--infofile", action="store", dest='infofile',default='', metavar='<infofile>', help="Required, define the PATH of infofile")
parser.add_argument("--geno", action="store", dest='geno',default='hg38', metavar='<geno>', help="Required, e.g., hg38 ")
parser.add_argument("--CPUS", action="store", dest='CPUS',default='4', metavar='<CPUS>', help="# of CPUS, default is 4 ")
parser.add_argument("--r1file", action="store", dest='r1file',default='', metavar='<r1file>', help="Required, define the path of r1file")
parser.add_argument("--r2file", action="store", dest='r2file',default='', metavar='<r2file>', help="Required, define the path of r2file")
parser.add_argument("--fqpair", action="store", dest='fqpair',default='', metavar='<fqpair>', help="Required, define the fqpair to retrieve record from design file")
parser.add_argument("--design", action="store", dest='design',default='', metavar='<design>', help="Required, specify the sample metadata")
parser.add_argument("--license", action="store", dest='license',default='', metavar='<license>', help="Required, define the Sentieon license server URL")
parser.add_argument("--cram", action="store", dest='cram',default='false', metavar='<cram>', help="Specify if re-calibrated cram file should be created after BQSR")

args=parser.parse_args()

def split_s3_path(s3_path):
    path_parts=s3_path.replace("s3://","").split("/")
    bucket=path_parts.pop(0)
    key="/".join(path_parts)
    return bucket, key

def split_gs_path(gs_path):
	path_parts=gs_path.replace("gs://","").split("/")
	bucket-path_parts.pop(0
	key='/'.join(path_parts)
	return bucket, key

# Assuming an OS-style container:
#   /app/Input will be the special mounted volume that oshell creates where the "Files" have been synced from s3 (or the directly mounted local OServer directory)
#   /app/Resource will be the special mounted volume that oshell created wehre the "Resources" have been synced to the compute node (or the directly mounted local OServer directory)
#   /app/Output  will be the special mounted volume for the working area that oshell with sync to "OutputFolder" at end (or the directly mounted local OServer directory)
# Seems we could parameterize these and require the RunEScript to pass in so it is more generic
genodir='/app/Resource/Genome/'
outdir='/app/Output/'
workdir=os.path.dirname(args.cfgfile)

###### in case of instance reruse ###############
# NO, if local OServer directory is mounted directly into container and multiple containers are running (parallel jobs), this would cause conflicts
# Instead, added check of local fize size below and skipped downloading if existing file is correct
#if os.path.exists( os.path.dirname(genodir)):
	#os.system('rm -r ' + genodir)
os.makedirs(os.path.dirname(genodir))

###### sync resource files ###############
# Seems this approach of having driver script sync based on genome.cfg is easier to "maintain" (just update genome.cfg) instead of specifying these files
#   in PScript's RunEScript Resources statement (also, there is limitation that Resources must all be from same folder)
print("------#### Syncing of resource files ####------")
gcp = storage.Client.create_anonymous_client()
s3 = boto3.client('s3')
dfgeno=pd.read_csv(args.cfgfile, sep='\t', header=0, comment='#')
# gnomAD is not needed for this step so skipping since it is a big file that may sync slowly depending on source/network
dfgeno=dfgeno[(dfgeno.GenomeVersion==args.geno) & (dfgeno.FileType != 'gnomAD')]
genoPATHS=dfgeno['URL'].values
for genoPATH in genoPATHS:
	#gunzip=dfgeno.loc[dfgeno['URL']==genoPATH, 'NeedsDecompression'].values[0]
	genodnPATH=genodir + ntpath.basename(genoPATH)
	if (os.path.exists(genodnPATH)):
		lsize = os.path.getsize(genodnPATH)
	else:
		lsize = -1
	print("Copying " + genoPATH + " to: " + genodnPATH)
	if re.search("^http", genoPATH):
		req = request.Request('genoPATH',method='HEAD')
		rsize = request.urlopen(req).headers['Content Length']
		if (lsize < 0 || lsize != rsize):
			request.urlretrieve(genoPATH, genodnPATH)
		else:
			print (genodnPATH + " already exists with the expected size")
	if re.search("^gs://", genoPATH):
		bucketG, keyG = split_gs_path(genoPATH)
		rsize = gcp.bucket(bucketG).get_blob(keyG).size
		if (lsize < 0 || lsize != rsize):
			with open(genodnPATH, 'wb') as dest:
				gcp.download_blob_to_file(genoPATH, dest)
		else:
			print (genodnPATH + " already exists with the expected size")
		# Since these are coming from Broads public repo and might "disappear" (they could take down or re-locate), printing path of archived copies to log as a data trail
		# Need to fill in the full archive path
		print("Copy of " + genoPATH + " is archived at s3://reference-files/")
	if re.search("^s3://", genoPATH):
		bucketG, keyG = split_s3_path(genoPATH)
		rsize = s3.head_object(Bucket=bucketG, Key=keyG)['ContentLength']
		if (lsize < 0 || lsize != rsize):
			s3.download_file(bucketG, keyG, genodnPATH)
		else:
			print (genodnPATH + " already exists with the expected size")
	#os.chdir(genodir)
	#if ".tar.gz" in genodnPATH:
	#	os.system("tar xvzf " + genodnPATH)
	#if gunzip:
	#	os.system("gunzip " + genodnPATH)
	
##### Parse sample metadata ######
print("------#### Parsing sample metadata ####------")
dfdesign=pd.read_csv(args.design, sep='\t', header=0, comment='#', na_values='.')
dfdesign=dfdesign[dfdesign.FQfilePair==args.fqpair]
SM=dfdesign['SampleID'].values[0]

LB=SM
if pd.notnull(dfdesign['Library'].values[0]):
  LB = LB + '.' + dfdesign['Library'].values[0]
fc=dfdesign['flowcell'].values[0]
ln=dfdesign['lane'].values[0]
bc=dfdesign['barcode'].values[0]
ID=SM
PU = LB
if pd.notnull(fc) :
  ID = ID + '.' + fc
  PU = PU + '.' + fc
if pd.notnull(ln) :
  ID = ID + '.' + ln
  PU = PU + '.' + ln
if pd.notnull(bc) :
  ID = ID + '.' + bc

###### run Sentieon.align.sh #######
print("------#### Invoking Sentieon.align.sh ####------")
os.chdir(workdir)
CPUS=str(args.CPUS)
fasta=dfgeno.loc[dfgeno['FileType']=='RefSeq', 'URL'].values[0].split("/").pop()
fastapath=genodir + fasta #re.sub("\.gz$","",fasta)

DBSNP=genodir + dfgeno.loc[dfgeno['FileType']=='dbSNP138', 'URL'].values[0].split("/").pop()
indels=dfgeno.loc[dfgeno['FileType']=='KnownIndels', 'URL'].values[0].split("/").pop()
indelspath=genodir + indels


# Unclear why this is a bash sub-script since downstream python "drivers" seem capable of invoking sentieon directly
CMD1="bash Sentieon.align.sh -s " + SM + ' -i ' + ID + ' -fq '+ fqpair + \
	' -f1 ' + args.r1file + ' -f2 ' + args.r1file + ' -pl ILLUMINA -pu ' + PU + ' -lb ' + LB + ' -o ' + outdir + ' -g ' + fastapath + \
	' -n ' + args.CPUS + ' -d ' + DBSNP + ' -k ' + indelspath + ' -ls ' + args.license + ' --write_cram ' + args.cram + ' 2>&1 | tee ' + outdir + fastq1 + '.log'
print("Assembled system command: \n\n" + CMD1)
os.system(CMD1)
