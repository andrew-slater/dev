import argparse
import pandas as pd

def split_s3_path(s3_path):
    path_parts=s3_path.replace("s3://","").split("/")
    bucket=path_parts.pop(0)
    key="/".join(path_parts)
    return bucket, key

###PARSE ARGUMENTS###
Epilog = """Example usage: python3 sampleinfo.py --designfile <designfile> --fqdir <fqdir>"""
parser = argparse.ArgumentParser(description="convert args.designfile to *.sampleinfo.", epilog=Epilog)
parser.add_argument("--designfile", action="store", dest='designfile',default='', metavar='<designfile>', help="Required, define the PATH of designfile")
args=parser.parse_args()

indir='/app/Input/'
outdir='/app/Output/'
	
def buildinfo(designfile,outdir):	
	dfsample= pd.read_csv(designfile, sep='\t', header=0, comment='#', na_values='.')
	print(dfsample)
	tumors=dfsample[dfsample.TumorOrNormal=='Tumor']
	tumorlist=tumors['FQfilePair'].values
	for tumor in tumorlist:
		dfsub=dfsample[dfsample.FQfilePair==tumor]
		# print(dfsub)
		outfile=outdir+'/'+str(tumor)+'.compareinfo'
		dfsub.to_csv(outfile, index=None, mode='w', sep='\t')

### download designfile ###
buildinfo(args.designfile,outdir)	
