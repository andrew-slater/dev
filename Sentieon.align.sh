#!/bin/bash

##### Constants

sleep 5

TITLE="System: $HOSTNAME"
RIGHT_NOW=$(date +"%x %r %Z")
TIME_STAMP="Running on $RIGHT_NOW by $USER"
nt=1

echo "#---- start Sentieon.sh ----#"
echo $TITLE
echo $TIME_STAMP

function usage()
{
# UNCLEAR why named 'realign' - when genericize to share across pipelines, re-name to "align"
    echo "usage: Sentieon.align.sh -s SM -i ID -fq fastqBaseName -f1 fastq1 -f2 fastq2 -pl PM -pu PU -lb LB -o OutputFolder -g RefSeqFasta -n numThreads -d dbSNPfile -k knownIndelsFile -ls licenseServer --write_cram true | [-h]"

}

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -s|--sample)
    SM="$2"
    shift
    shift
    ;;
    -i|--id)
    ID="$2"
    shift
    shift
    ;;
    -fq|--fastq)
    fastq="$2"
    shift
    shift
    ;;
    -f1|--fastq1)
    fastq_1="$2"
    shift
    shift
    ;;
    -f2|--fastq2)
    fastq_2="$2"
    shift
    shift
    ;;
    -pl|--platform)
    PL="$2"
    shift
    shift
    ;;
    -pu|--platunit)
    PU="$2"
    shift
    shift
    ;;
    -lb|--library)
    LB="$2"
    shift
    shift
    ;;
    -o|--output)
    outdir="$2"
    shift
    shift
    ;;
    -g|--genome)
    fasta="$2"
    shift
    shift
    ;;
    -n|--tasknumber)
    nt=$2
    shift
    shift
    ;;
    -d|--dbsnp)
    dbsnp=$2
    shift
    shift
    ;;
    -k|--known_indels)
    known_indels=$2
    shift
    shift
    ;;
    -ls|--license_server)
    license_server=$2
    shift
    shift
    ;;
    -c|--write_cram)
    write_cram=$2
    shift
    shift
    ;;
    -h|--help)
    shift
    usage
    ;;
    *)
    usage
    exit
    shift
    ;;
esac
done


# A common error might be a customer failing to allow communication between their Sentieon license server and the AWS compute nodes they have configured on their OServer
export SENTIEON_LICENSE="$license_server"

workdir="$outdir/$fastq"
mkdir -p $workdir
cd $workdir

# This workflow is an attempt to reconcile Sentieon's recommended best practices(https://github.com/Sentieon/sentieon-scripts/blob/master/example_pipelines/germline/wgs.sh)
#  with this proposed community standard (https://github.com/CCDG/Pipeline-Standardization/blob/master/PipelineStandard.md)
#  Note, this workflow only performs the alignment pre-processing that is shared between germline and somatic pipelines (i.e. BAM file generation in preparation for variant discovery).
#   The link to Sentieon's best practices is for their germline pipeline where the final step (variant discovery Haplotyper) is beyond the scope of this pre-processing workflow.
#  Note, the CCDG's proposed community standard did not achieve wide adoption but has merit (i.e. primarily the standardization of reference sequence fasta to ensure contig ID compatibility)
#  Specific decisions RE: reconciliation are in the comments for each step
#  Deviations from Sentieon's best practices to achive the CCDG's prescriptions are based on Sentieon's documentation here: https://support.sentieon.com/appnotes/functional_equivalent/

# --------------------------------------------
# 1. alignment for samples
# --------------------------------------------
echo "#---- 1. alignment for samples ----#"

# CCDG prescribes including the .alt index file for the reference sequence to enable ALT-aware aligning but Sentieon's indexing guidance (https://support.sentieon.com/manual/appendix/troubleshooting/#preparing-reference-file-for-use)
#   does not mention the .alt index. Sentieon confirmed their implementation of bwa is capable of ALT-aware aligning when the .alt index is provided.
# Although the use of ALT-aware aligning seems irrelevant since the CCDG does not prescribe the necessary post-processing to leverage the ALT-aware results (https://github.com/lh3/bwa/blob/master/README-alt.md),
#   disabling ALT-aware aligning without also removing the ALT contigs from the reference fasta would likely give non-ideal results as those ALT contigs would effectively act as decoy sinks
#   where reads could primarily align and thus get lost. Therefore, it seems the CCDG's prescription to perform ALT-aware aligning was because they did not want to prescribe a new reference fasta with the ALT contigs removed.
# So, choosing to follow the CCDG's standard and including the .alt index to enable ALT-aware aligning.

# CCDG prescribes not using -M but Senteon's best practices suggest using it to ensure the alignments are compatible with older versions of GATK/Picard (it seems these did not recognize the "new" Supplementary alignment flag).
#   If we consider those older versions of GATK/Picard to be obsolete, it seems more appropriate to embrace the CCDG standard so not using -M.

# CCDG prescribes -K of 100m but Sentieon best practices suggest 10m. Per Sentieon, the 10m value avoids memory errors and has minimal impacts on the final results so using 10m.

# CCDG prescribes using -Y but Sentieon's best practices do not include this flag. Unclear why CCDG prescribes this (perhaps to facilitate optional downstream processing of these supplementary alignements)
#   but it seems using -Y has no functional effects (clipped portions are ignored regardless of hard/soft state) other than inflating the size of the BAM file a bit so including this flag.

# CCDG prescribes the RG should contain the ID, SM, PL, PU, and LB fields and recommends the CN field. Specifically:
#   SM should contain the subject identifier (i.e. not a sample or aliquot ID)
#   PU should be of the form sample.LB.flowcell.lane but sub-fields which don't vary within the BAM file can be omitted
#   LB should not be omitted even if it is a single non-variable value for the whole BAM file
#   PM is not required but if included should follow this CV: “HiSeq-X”, “HiSeq-4000”, “HiSeq-2500”, “HiSeq-2000”, “NextSeq-500”, or “MiSeq”
# Sention advises it should contain the ID, SM, PL, PU, and LB fields. Specifically:
#   ID should be of the form SM.flowcell.lane.barcode
#   SM is used to jointly analyze all reads from the same sample and therefore should be an appropriate ID (e.g. aliquot ID)
#   PL is required for BQSR to determine which error model to use and only ILLUMINA and IONTORRENT are supported
#   PU should be of the form flowcell.lane but can be omitted if it doesn't vary within the BAM file
#   LB should be of the form SM.library but can be ommitted if it doesn't vary within the BAM file
#   from: https://support.sentieon.com/appnotes/read_groups/
# It seems these RG values can easily be updated as a post-processing step (e.g. using SAMtools) and they are not widely used as a mechanism of maintaining a data trail. So,
#   Not using the CN field recommended by CCDG
#   ID will follow Sentieon's recommendations and be assembled as SM.flowcell.lane.barcode contingent on user specifying each sub-field's value (i.e. only SM is required)
#   SM will follow Sentieon's recommendations as it seems functionally relevant (i.e. using a subject ID as CCDG prescribes would probably be inappropriate for pooling reads)
#   PL will be fixed at ILLUMINA for Sentieon's BQSR requirements (IONTORRENT is the only other option supported and enabling this platform may require other pipeline modifications)
#   PU will follow CCDG's prescription and thus minimally contain SM.library with the flowcell and lane sub-fields being optional
#   LB will follow CCDG's prescription to be non-missing and follow Sentieon's recommended form of SM.library if the user specifies library, otherwise it will simply be SM

# The -r option for sort is optional but suspect it uses it to define contig order and thus provide a more standardized output BAM
sentieon bwa mem -Y -R "@RG\tID:$ID\tSM:$SM\tPL:$PL\tPU:$PU\tLB:$LB" -t $nt -K 10000000 "$fasta" "$fastq_1" "$fastq_2" | sentieon util sort -o "$fastq"_bwa.bam -t $nt -r "$fasta" --sam2bam -i -
# Do we need to set the bwt_max_mem variable? Sentieon doc says it defaults to 24 GB so if have more available and want to raise to have go faster, need to set (2x and 4x large both have more than 24 GB)
# sort has --temp-dir option which seems highly relevant to sorting - says defaults to CWD, is that appropriate?
#   does the SENTIEON_TMPDIR environment variable (set in docker image) get used if this isn't specified (instead of CWD ?)


# --------------------------------------------
# 2. bwa alignment summary
# --------------------------------------------
echo "#---- 2. bwa alignment summary ----#"

# CCDG does not prescribe any particular QC summarization so following Sentieon best practices

sentieon driver -r $fasta -t $nt -i "$fastq"_bwa.bam --algo MeanQualityByCycle "$fastq"_mq_metrics.txt --algo QualDistribution "$fastq"_qd_metrics.txt --algo GCBias --summary "$fastq"_gc_summary.txt "$fastq"_gc_metrics.txt --algo AlignmentStat "$fastq"_aln_metrics.txt --algo InsertSizeMetricAlgo "$fastq"_is_metrics.txt
sentieon plot GCBias -o "$fastq"_gc-report.pdf "$fastq"_gc_metrics.txt
sentieon plot QualDistribution -o "$fastq"_qd-report.pdf "$fastq"_qd_metrics.txt
sentieon plot MeanQualityByCycle -o "$fastq"_mq-report.pdf "$fastq"_mq_metrics.txt
sentieon plot InsertSizeMetricAlgo -o "$fastq"_is-report.pdf "$fastq"_is_metrics.txt
# These are only the first 5 QC algos, there are 6 others, have they been excluded in favor of OS's NgsQCWizard proc? Or only relevant to WES (which might still be relevant for this pipeline, we haven't specified it is WGS only but WES requires interval target list which we haven't parameterized). Or perhaps selected these 5 based on Sentieon doc examples (look to see if they have other examples where the other algos used)
#   HsMetricAlgo - WES only?
#   CoverageMetrics - Sentieon doc says should occur after de-duplication
#   BaseDistributionByCycle
#   QualityYield - this has 2 options re: inclusion of secondary & supplementary reads so perhaps is similar to OS DnaSeqQCMetrics proc ?
#   WgsMetricsAlgo - WGS only, seems at least some overlap with OS DnaSeqQCMetrics proc
#   SequenceArtifactMetricsAlgo - relevant to Oxford Nanopore only ?

# --------------------------------------------
# 3. Remove Duplicate Reads for sample
# --------------------------------------------
echo "#---- 3. Remove Duplicate Reads for sample ----#"

# CCDG prescribes a set of criteria for defining duplicates
# Per Sentieon, to achieve this, need to use this "3-step" approach instead of the simpler "2-step" in their best practices
# Believe this may be because their simpler "2-step" approach only marks duplicated primary reads and ignores the non-primary reads
#   and this 3-step approach marks the non-primary reads as well per the CCDG's prescription

sentieon driver -t $nt -i "$fastq"_bwa.bam --algo LocusCollector --fun score_info "$fastq"_score.txt
sentieon driver -t $nt -i "$fastq"_bwa.bam --algo Dedup --score_info "$fastq"_score.txt --metrics "$fastq"_dedup_metrics.txt --output_dup_read_name "$fastq"_qname.txt
sentieon driver -t $nt -i "$fastq"_bwa.bam --algo Dedup --dup_read_name "$fastq"_qname.txt "$fastq"_deduped.bam 


# Step 4. Indel realigner for sample
echo "#---- 4. Indel realigner for sample ----#"

# CCDG suggests skipping this step as low value considering cost:benefit (computational cost)
# Sentieon's best practices don't include this step either and it is listed as optional in their manual: https://support.sentieon.com/versions/202010.03/manual/DNAseq_usage/dnaseq/#indel-realignment-optional
# However, it does have some value in that correctly aligned indels will ensure better score recalibation in next step (BQSR)
# And since Sentieon's solution minimzes the computation cost, deciding to include this step

sentieon driver -r $fasta -t $nt -i "$fastq"_deduped.bam --algo Realigner -k $known_indels "$fastq"_realigned.bam


# --------------------------------------------
# 5. Base recalibration for sample
# --------------------------------------------
echo "#---- 5. Base recalibration for sample ----#"

# CCDG prescribes this step be done with 3 fixed/static sources of known variants:
#  1) dbSNP138: Homo_sapiens_assembly38.dbsnp138.vcf
#  2) 1000G gold standard indels: Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
#  3) known indels: Homo_sapiens_assembly38.known_indels.vcf.gz
#  These files were sourced from the Broad's resource bundle and left-normelized with BCFtools
#  The 2 indel files were compared after normalization and Homo_sapiens_assembly38.known_indels.vcf.gz is a superset of Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
#    with the exception of 6 alleles unique to Mills_and_1000G_gold_standard.indels.hg38.vcf.gz 
#  Decided this was a small enough number to ignore such that we won't be using the Mills_and_1000G_gold_standard.indels.hg38.vcf.gz source
# CCDG prescribes compressing the scores to minimize the CRAM file size
# Sentieon's best practices have the creattion of the re-calibrated BAM/CRAM file as optional since subsequent variant discovery steps can "apply" the re-calibrated scores to the un-recalibrated bam file at runtime
# Therefore, deciding to make the creation of the CCDG-compliant CRAM file (with compressed scores) an option for the user with default being to not create this file

sentieon driver -r $fasta -t $nt -i "$fastq"_realigned.bam --algo QualCal -k $dbsnp -k $known_indels "$fastq"_recal_data.table

shopt -s nocasematch
if [[ "${write_cram}" =~ ^true$|^t$ ]]; then
  echo "#---- Optionally writing re-calibrated CRAM file with MQ tags added by samblaster ----#"
  
  # CCDG prescribes MC and MQ tags and Sentieon's bwa only includes MC  
  # In Sentieon's documentation for how to comply with CCDG, they assume this final cram file creation step will occur and add this samblaster step b/w the original bwa mem alignment and sorting in step 1
  # But as this will likely be a long-running process for WGS samples, decided to "reserve" this samblaster step for jobs where the user specifies they want the cram file
  # sentieon driver won't take stdin so need to convert the samblaster output to a bam file and sentieons util sort seems most efficient way (don't believe an actual sort is necessary)
  samtools view -h "$fastq"_realigned.bam | samblaster --addMateTags -a | sentieon util sort -o "$fastq"_blasted.bam -t $nt -r "$fasta" --sam2bam -i -
  sentieon driver -r $fasta -t $nt -i "$fastq"_blasted.bam --read_filter QualCalFilter,table="$fastq"_recal_data.table,prior=-1.0,indel=false,levels=10/20/30,min_qual=6 --algo ReadWriter "$fastq".cram
fi
shopt -u nocasematch

# CCDG does not prescribe any particular QC summarization so following Sentieon best practices
sentieon driver -r $fasta -t $nt -i "$fastq"_realigned.bam -q "$fastq"_recal_data.table --algo QualCal -k $dbsnp -k $known_indels "$fastq"_recal_data.table.post
sentieon driver -t $nt --algo QualCal --plot --before "$fastq"_recal_data.table --after "$fastq"_recal_data.table.post "$fastq"_recal.csv
sentieon plot QualCal -o "$fastq"_recal_plots.pdf "$fastq"_recal.csv


# keep sleeping 5s for debug
echo "keep sleeping 5 seconds for debug"
sleep 5


# ---------------------------------------------
# 6. reorgnize results
# ----------------------------------------------
# Why not just write these to final destination to begin with ?
mkdir -p $workdir/bam
mkdir -p $workdir/qcplots
mkdir -p $workdir/recal_bam
mkdir -p $workdir/qc_tables
mv $workdir/*recal_data* $workdir/recal_bam/
# Need this "final" bam in same folder as the recal_data.table so can sync as Resource to container running subsequent variant discovery steps
mv $workdir/*_realigned.bam* $workdir/recal_bam/
mv $workdir/*.bam* $workdir/bam/
mv $workdir/*.pdf $workdir/qcplots/
mv $workdir/*.txt* $workdir/qc_tables/
mv $workdir/*.csv $workdir/qc_tables/

echo "#---- end Sentieon.sh ----#"
