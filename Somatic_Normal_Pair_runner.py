import os
#import re
import ntpath
#import sys
import argparse
import boto3
#import glob
import pandas as pd
from google.cloud import storage


###PARSE ARGUMENTS###
Epilog = """Example usage: python3 Somatic_Normal_Pair_runner.py"""
parser = argparse.ArgumentParser(description="download sample fastq and genome ref.", epilog=Epilog)
parser.add_argument("--cfgfile", action="store", dest='cfgfile',default='', metavar='<cfgfile>', help="Required, define the PATH of genome cfg file")
parser.add_argument("--geno", action="store", dest='geno',default='hg38', metavar='<geno>', help="Required, e.g., hg38 ")
parser.add_argument("--CPUS", action="store", dest='CPUS',default='4', metavar='<CPUS>', help="# of CPUS, default is 4 ")
parser.add_argument("--comparefile", action="store", dest='comparefile',default='', metavar='<comparefile>', help="Required, define the PATH of genome comparefile")
parser.add_argument("--prjdir", action="store", dest='prjdir',default='', metavar='<prjdir>', help="Required, define the PATH of genome prjdir")
args=parser.parse_args()

######### clean the output in case reuse ###########
# this would only work if running on an AWS node such that /app/Output is mounting of a temporary "write-only" node directory (i.e. expected to be empty) that will be copied to the "real" OutputFolder at end of RunEScript proc
# if running on local OServer, the local "real" OutputFolder path is mounted directly and this would catastrophically destroy the files already there
# os.system('rm -rf /app/Output/*')

def split_s3_path(s3_path):
	path_parts=s3_path.replace("s3://","").split("/")
	bucket=path_parts.pop(0)
	key="/".join(path_parts)
	return bucket, key

def dn_file(s3filepath, outdir):
	s3 = boto3.client('s3')
	bucketG, keyG = split_s3_path(s3filepath)
	print(bucketG,keyG)
	newfile=outdir + ntpath.basename(s3filepath)
	s3.download_file(bucketG, keyG, newfile)
	
def dn_bam_recall(dirpath, NAME, outdir):	
	dirpath=dirpath.replace('%%','s3:/')
	bamfile=dirpath+'/Sentieon_output/'+NAME+'/bam/'+NAME+'_realigned.bam'
	baifile=dirpath+'/Sentieon_output/'+NAME+'/bam/'+NAME+'_realigned.bam.bai'
	tblfile=dirpath+'/Sentieon_output/'+NAME+'/RECAL_DATA/'+NAME+'_recal_data.table'	
	dn_file(bamfile, outdir)
	dn_file(baifile, outdir)
	dn_file(tblfile, outdir)

cfgfile=args.cfgfile
prjdir=args.prjdir
geno=args.geno
CPUS=args.CPUS
comparefile=args.comparefile

indir='/app/Input/'
#dndir='/app/Input/bam/'
genodir='/app/Resource/Genome/'
resdir='/app/Resource/'
outdir='/app/Output/'
print("################### Start somatic pairing ###############")
###### in case of instance reruse ###############
#if os.path.exists(os.path.dirname(dndir)):
#	os.system('rm -r '+dndir)	
#os.makedirs(os.path.dirname(dndir))
if os.path.exists( os.path.dirname(genodir)):
	os.system('rm -r '+genodir)
os.makedirs(os.path.dirname(genodir))

##### read comparefile #####
print('cfgfile is: ', cfgfile)
print('prjdir is: ', prjdir)
print('geno is: ', geno)
print('comparefile is: ', comparefile)
dfcompare=pd.read_csv(comparefile, sep='\t', header=0, comment='#')
print(dfcompare)
treatsam=dfcompare.Tumor.unique()[0]
consam=dfcompare.Normal.unique()[0]
print(treatsam)
print(consam)
print("------#### Downloading of bams ###------")
#dn_bam_recall(prjdir, treatsam, indir)
#dn_bam_recall(prjdir, consam, indir)		
		
###### download genome ref ###############
print("------#### Downloading of genomes ###------")
#s3 = boto3.client('s3')
gcp = storage.Client.create_anonymous_client()
dfgeno=pd.read_csv(args.cfgfile, sep='\t', header=0, comment='#')
# gnomAD is not needed for this step so skipping since it is a big file that syncs slow from http
dfgeno=dfgeno[dfgeno.GenomeVersion==args.geno]
genoPATHS=dfgeno['URL'].values
for genoPATH in genoPATHS:
	genodnPATH=genodir + ntpath.basename(genoPATH)
	print(genodnPATH)
	if re.search("^http", genoPATH):
		request.urlretrieve(genoPATH, genodnPATH)
	if re.search("^gs://", genoPATH):
		with open(genodnPATH, 'wb') as dest:
			gcp.download_blob_to_file(genoPATH, dest)

###### run Sentieon.Germline_mutation.sh #######
os.chdir(resdir)
CPUS=str(CPUS)
tumor_sample=treatsam
normal_sample=consam
#BWA=genodir+'/BWAIndex/genome.fa'
fasta=dfgeno.loc[dfgeno['FileType']=='RefSeq', 'URL'].values[0].split("/").pop()
fastapath=genodir + fasta #re.sub("\.gz$","",fasta)
#comfile=genodir+'/'+geno+'_Cosmic.vcf.gz'
#DBSNP=genodir+'/'+geno+'_dbsnp.vcf.gz'
DBSNP=genodir + dfgeno.loc[dfgeno['FileType']=='dbSNP138', 'URL'].values[0].split("/").pop()
#INDEL=genodir+'/'+geno+'_known_indels.vcf.gz'
indels=dfgeno.loc[dfgeno['FileType']=='KnownIndels', 'URL'].values[0].split("/").pop()
indelspath=genodir + indels


CMD1="sentieon driver -t " + CPUS + " -r " + fastapath +" -i "+indir+'/'+tumor_sample+"_realigned.bam -i "+indir+'/'+normal_sample+"_realigned.bam -q "+indir+'/'+tumor_sample+"_recal_data.table -q "+indir+'/'+normal_sample+"_recal_data.table --algo Realigner -k "+indelspath+" "+outdir+'/'+tumor_sample+"_tn_corealigned.bam" +' 2>&1 | tee '+ outdir+'/'+tumor_sample+'.corealigned.log'

print(CMD1)
os.system(CMD1)

CMD2="sentieon driver -t " + CPUS + " -r " + fastapath +" -i "+outdir+'/'+tumor_sample+"_tn_corealigned.bam --algo TNsnv --tumor_sample "+tumor_sample+" --normal_sample "+normal_sample+" --dbsnp "+DBSNP+ " --call_stats_out "+outdir+'/'+tumor_sample+"_output-call.stats" + " "+outdir+'/'+tumor_sample+"-tnsnv.vcf.gz"+' 2>&1 | tee '+ outdir+'/'+tumor_sample+'.pair_TNsnv.log'

print(CMD2)
os.system(CMD2)

CMD3="sentieon driver -t " + CPUS + " -r " + fastapath +" -i "+outdir+'/'+tumor_sample+"_tn_corealigned.bam --algo TNhaplotyper --tumor_sample "+tumor_sample+" --normal_sample "+normal_sample+" --dbsnp "+DBSNP + " "+outdir+'/'+tumor_sample+"-tnhaplotyper.vcf.gz"+' 2>&1 | tee '+ outdir+'/'+tumor_sample+'.pair_TNhaplotyper.log'

print(CMD3)
os.system(CMD3)

#### orgnize files ####
sampdir=outdir+'/'+tumor_sample
if os.path.exists(os.path.dirname(sampdir)):
	os.system('rm -rf '+sampdir+'/*')
os.system("mkdir -p "+sampdir+'/Vcf')
os.system("mkdir -p "+sampdir+'/bam')
os.system("mkdir -p "+sampdir+'/qc_tables')

os. chdir(outdir)
os.system("mv *.bam "+sampdir+"/bam/")
os.system("mv *.bam.bai "+sampdir+"/bam/")
os.system("mv *.vcf.gz "+sampdir+"/Vcf/")
os.system("mv *.tbi "+sampdir+"/Vcf/")
os.system("mv *.stats "+sampdir+"/qc_tables/")
